from flask import Flask, request
from flask import jsonify
from pickle import load
import numpy as np


app = Flask(__name__)

mdl = load(open('model.pkl', 'rb'))
scaler = load(open('scaler.pkl', 'rb'))

@app.route('/predict/', methods=['POST'])
def welcome():
    r_dict = request.get_json()
    pr = np.array([[r_dict['sales'], r_dict['profit']]])
    tst = scaler.transform(pr)
    y_pred = mdl.predict(tst)
    n_outliers = np.count_nonzero(y_pred == 1)

    return jsonify({
        'input': {
            'sales': r_dict['sales'], 
            'profit': r_dict['profit']
        }, 
        'result': {
            'profit_percentage': round(r_dict['profit'] * 100 / r_dict['sales'], 1),
            'anomaly': bool(n_outliers)
        }
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)